#!/bin/bash
#
# create set of solutions
# by dendys
#
# $1 - amount of fields on trinagle edge

if [[ $# -eq 0 ]]; then
    echo 'amount of fileds on triangle edge expected: 3, 4, 5, 6, 7'
    exit 1
fi


if [ $1 -eq 3 ]; then
    echo triangle 3
    START_POSITIONS="113 212 122 311 221 131"
    #START_POSITIONS="113"
elif [ $1 -eq 4 ]; then
    echo triangle 4
    START_POSITIONS="114 213 123 312 222 132 411 321 231 141"
    #START_POSITIONS="213"
elif [ $1 -eq 5 ]; then
    echo triangle 5
    START_POSITIONS="115 214 124 313 223 133 412 322 232 142 511 421 331 241 151"
    #START_POSITIONS="115"
elif [ $1 -eq 6 ]; then
    echo triangle 6
    START_POSITIONS="116 224 125 134 233 143 242 152 161"
    #START_POSITIONS="116"
elif [ $1 -eq 7 ]; then
    echo triangle 7
    START_POSITIONS="117 225 333 126 135 234 144"
#    START_POSITIONS="117" #no solution can be find
else
    echo no or invalid parameter ... stopping
    exit 1
fi

CNT_FIELDS=$1


for POS in $START_POSITIONS; do

    FILE_NAME=solutions-${CNT_FIELDS}-${POS}
    echo $POS
    ./troj >$FILE_NAME << EOF
$CNT_FIELDS
$POS
EOF

done

exit 0
