# triangle

Triangle game solution command line searcher.

## PLay fields

List of triangle play fields with their numerical representation.

0. Triangle with 3 fields on the edge:

                    113
                  212 122
                311 221 131

1. Triangle with 4 fields on the edge:

                    114
                  213 123
                312 222 132
              411 321 231 141

2. Triangle with 5 fields on the edge:   

                    115
                  214 124
                313 223 133
              412 322 232 142
            511 421 331 241 151

3. Triangle with 6 fields on the edge:

                    116
                  215 125
                314 224 134
              413 323 233 143
            512 422 332 242 152
          611 521 431 341 251 161

4. Triangle with 7 fields on the edge:

                    117
                  216 126
                315 225 135
              414 324 234 144
            513 423 333 243 153
          612 522 432 342 252 162
        711 621 531 441 351 261 171

## Compilation

To compile run `gcc -Wall -pedantic -o troj troj.c`.
In order to find mem leaks it is CMemLeak used and also mtrace.
Before compilation uncomment _DEBUG define and mcheck.h and CMemLeak.h include
and also uncomment mtrace() function call.
Then build with `gcc -Wall -pedantic -g -o troj troj.c CMemLeak.c`.
You need also set env variable `export MALLOC_TRACE=memory.log`.
Afterwards you can see result in CMemLeak.txt or `mtrace troj memory.log`

## Run

To run `./troj`. Introduced `./cresol.sh` that receives one parameter - amount of fileds on edge
and run troj for almost what you set in variable START_POSITIONS in script.