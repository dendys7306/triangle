/******************************************************************
 *                                                                *
 *   TRIANGLE GAME - freeware no (c)                              *
 *   -------------------------------                              *
 *                                                                *
 *   This programm searches for solution of game based on         *
 *   triangular play field. This fields is represented by three   *
 *   dimensional array.                                           *
 *                                                                *
 *   Example of play field for 5 fields on edge:                  *
 *                                                                *
 *                           115                                  *
 *                         214 124                                *
 *                       313 223 133                              *
 *                     412 322 232 142                            *
 *                   511 421 331 241 151                          *
 *                                                                *
 *   At the beginning are ( all - 1 ) play fields with stones.    *
 *   Only start field is empty. Player can do these types         *
 *   of moves:                                                    *
 *                                                                *
 *         * o o  =>  o * *     or     o o *  =>  * * o           *
 *                                                                *
 *   Where ( o ) represents play field with stone and ( * )       *
 *   represents empty play field.                                 *
 *                                                                *
 *   User should enter two values:                                *
 *                                                                *
 *      amount of fields for one edge of ractangel: 1 - 9         *
 *      start position in form of three numbers: f.e. 115         *
 *                                                                *
 *   Current version: 1.4                                         *
 *   Modification: 02.12.2018                                     *
 *   Description: corrected all known bugs                        *
 *                                                                *
 *   Version: 1.3                                                 *
 *   Modification: 03.07.2001                                     *
 *   Description: Functional version, returns to <stdout>         *
 *                found solutions and on the end their amount.    *
 *                Searchs state space until reachs first level.   *
 *                                                                *
 *   Version: 1.2                                                 *
 *   Modification: 01.07.2001                                     *
 *   Description: Comments and renaming of functions and          *
 *                variables                                       *
 *                                                                *
 *   Version: 1.0                                                 *
 *   Modification: 30.6.2001                                      *
 *   Description: First functional version.                       *
 *                                                                *
 *   Initial creation: 22.05.2001                                 *
 *   Author: Daniel Antos                                         *
 *   Email: daniel.antos@ifs-it.de                                *
 *                                                                *
 *                                                                *
 ******************************************************************/

//#define _DEBUG

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>

//#include <mcheck.h>
//#include "CMemLeak.h"


#define MAX_FIELDS_OF_EDGE                  9
#define CHARS_IN_MOVE                       9
#define CHARS_IN_INDEX                      3

//assume to be calculated values in future
#define PRINT_RESULT_FOR_EACH               500
#define PRINT_RESULT_FOR_EACH2              50000
#define FORCE_END_AFTER_MOVE_COUNT          10000000
#define BOUNDARIES_RANGE                    3000000


#define TEXT_INPUT_FIELDS                   "Enter amount of fields for one edge: "
#define TEXT_FIELDS_AND_POSSIBLE_MOVES      "\nAmount of all play fields and possible moves: \n" 
#define TEXT_RESULT_WAS_FOUND               "\nBINGO - solution was found (%d.)!\n" 
#define TEXT_INPUT_INDEX_OF_START_POSITION  "Enter index of start position [nnn]: "
#define TEXT_SUMMARY_OF_POSSIBLE_MOVES      "\nSummary of possible moves:\n"
#define TEXT_AMOUNT_OF_POSSIBLE_MOVES       "\nAmount of possible moves: %d\n"
#define TEXT_REPORT_SOLUTION                "\nAmount of found solutions is %d.\n"
#define TEXT_REPORT_OF_TOTAL_FORWARD_LEVELS "Amount of total forward move levels is %d.\n"
 
#define ERROR_MSG_BAD_COUNT_OF_FIELDS       "ERROR - wrong amount of play fields for one edge!\n"
#define ERROR_MSG_TOO_MANY_FIELDS           "ERROR - amount of fields must be >= 3 and <=9!\n" 
#define ERROR_MSG_BAD_START_POSITION        "ERROR - entered wrong index of start position!\n"
#define ERROR_MSG_BAD_INDEX                 "ERROR - invalid start index, must be in form [nnn]!\n"
 
#define ERROR_NUM_BAD_COUNT_OF_FIELDS       101
#define ERROR_NUM_TOO_MANY_FIELDS           102
#define ERROR_NUM_BAD_START_POSITION        103
#define ERROR_NUM_BAD_INDEX                 104


typedef struct moveRecord
{

    int move[ CHARS_IN_MOVE ];
    struct moveRecord *pNext;

} listOfMoves;


typedef struct moveLevelRecord{
    int move[ CHARS_IN_MOVE ];
    struct moveLevelRecord *pNext, *pPrev, *pBeginningOfLevel;
} listOfLevels;



void printMove( int pMove[] )
{
    int i;

    printf( " " );

    for ( i = 0; i < CHARS_IN_MOVE; i++ )
    {
        printf( "%d", pMove[i] );
    }
    printf( " " );
}


void printStateOfGame( listOfLevels *pList )
{
    int i = 1;
    printf( "%2d. ", i );

    while ( pList->pNext != NULL ) 
    {

        printMove( pList->move );
	// useful printing of address
        //printf( " <-%x,->%x,<<-%x  ", pList->pPrev, pList->pNext, pList->pBeginningOfLevel );

        if ( pList->pBeginningOfLevel != NULL )
        {
            printf( "\n" ); 
            i++;
            printf( "%2d. ", i );
        }

        pList = pList->pNext;

    }
    fflush(stdout);
}

void doMove( int pFields[ MAX_FIELDS_OF_EDGE ][ MAX_FIELDS_OF_EDGE ][ MAX_FIELDS_OF_EDGE ], 
             int *pMove )
{
    int h1, h2, h3;

    h1 = !pFields[ pMove[0] ][ pMove[ 1 ] ][ pMove[ 2 ] ];
    h2 = !pFields[ pMove[3] ][ pMove[ 4 ] ][ pMove[ 5 ] ];
    h3 = !pFields[ pMove[6] ][ pMove[ 7 ] ][ pMove[ 8 ] ];
    pFields[ pMove[0] ][ pMove[ 1 ] ][ pMove[ 2 ] ] = h1; 
    pFields[ pMove[3] ][ pMove[ 4 ] ][ pMove[ 5 ] ] = h2;
    pFields[ pMove[6] ][ pMove[ 7 ] ][ pMove[ 8 ] ] = h3;
}

void backStep( int pFields[ MAX_FIELDS_OF_EDGE ][ MAX_FIELDS_OF_EDGE ][ MAX_FIELDS_OF_EDGE ], 
              listOfLevels *pLastMoveInLevel, 
              int *plevel,
	      int counter )
{
    listOfLevels *before, *after, *pLastMove, *pTempMove;
    int tempInt;
    
    /* ---- Set pLastMove to point to last realised move in current level.
     */
    assert ( pLastMoveInLevel->pBeginningOfLevel != NULL );
    pLastMove = pLastMoveInLevel->pBeginningOfLevel;
    
    /* ---- Step back. Undo last move.
     */
    assert ( pLastMove != NULL );

    doMove( pFields, pLastMove->move );

    if ( pLastMove->pBeginningOfLevel != NULL )
    {

        tempInt = *plevel;
        tempInt--;
        *plevel = tempInt;

        //printf( " L%d ", tempInt );

    	if ( tempInt == 0 ){
    	    return;
    	}

        /* ---- Do recursively back step, if the there is olny 
         *      one step in level and no playable moves.
         */
        assert ( pLastMoveInLevel->pPrev != NULL );

        //printf( " << " );

        backStep( pFields, pLastMoveInLevel->pPrev, plevel, counter );

    }
    else
    {
        assert( pLastMove->pNext != NULL );
        pLastMoveInLevel->pBeginningOfLevel = pLastMove->pNext;

        if ( pLastMove->pPrev != NULL ) {
            before = pLastMove->pPrev;
        }

        after = pLastMove->pNext;

        assert( pLastMove != NULL );

        pTempMove=pLastMove;

        if ( *plevel == 1 ){
            pLastMove->pPrev = NULL;
        }

        if ( pLastMove->pPrev != NULL ) {
            before->pNext = after;
            after->pPrev = before;
        }

        //printf( " < " );
	
        doMove( pFields, after->move );

        if ( *plevel != 1 ) {
            free( pTempMove );
        }
    }
}


void assignMove( int *pSource, 
                 int *pTarget )
{
    int i;

    for ( i = 0; i < CHARS_IN_MOVE; i++ )
    {
        pTarget[ i ] = pSource[ i ];
    }
}

void printOfPossibleMoves( listOfMoves *pMove )
{
    int i = 0;

    while ( pMove->pNext != NULL ) 
    {
        i++;
        printf( "%2d.", i);
        printMove( pMove->move );
        printf( "\t" );
        pMove = pMove->pNext;
    }    
}

void setBoundaries( int *pMinLevel, int *pLastMinLevel, int currLevel ){

    if ( currLevel < *pLastMinLevel ) {
        if ( currLevel < *pMinLevel ) {
            *pMinLevel = currLevel;
        }
    }   
    *pLastMinLevel = currLevel;
}

int main( void )
{
    int i, j, k, edge, sumOfIndexes, amountOfFields = 0, 
        amountOfMoves, counter = 0, counterA = 0;
    int end = 0, suma, countOfMoves, level = 0, oldLevel;
    int AllFields[ MAX_FIELDS_OF_EDGE ][ MAX_FIELDS_OF_EDGE ][ MAX_FIELDS_OF_EDGE ];
    int startIndex, indexOfField[ CHARS_IN_INDEX ], amountOfResults = 0;
    /* heuristic variables */
    int minLevel, lastMinLevel = 0, boundaryCounter = 0;

    listOfMoves  *pPossMove, *pFirstMove, possMove;

    listOfLevels *pRealMove, *pFirstLevel, *pFirstMoveInLevel, 
                 *pTempAMove = NULL, *pTempMove = NULL, realMove;

    //mtrace();

    printf( TEXT_INPUT_FIELDS );
    scanf( "%d", &edge );

    if ( edge < 3 )
    {
        printf( ERROR_MSG_BAD_COUNT_OF_FIELDS );
        return ERROR_NUM_BAD_COUNT_OF_FIELDS;
    }

    if ( edge > 9 )
    {
        printf( ERROR_MSG_TOO_MANY_FIELDS );
        return ERROR_NUM_TOO_MANY_FIELDS;
    }

    printf( TEXT_INPUT_INDEX_OF_START_POSITION );
    scanf( "%d", &startIndex );

    if ( startIndex < 100 || startIndex > 999 )
    {
        printf( ERROR_MSG_BAD_INDEX );
        return ERROR_NUM_BAD_INDEX;
    }
    
    indexOfField[ 2 ] = ( startIndex % 100 ) % 10 ;
    indexOfField[ 1 ] = ( ( startIndex % 100 ) - indexOfField[ 2 ] ) / 10;
    indexOfField[ 0 ] = ( startIndex - ( ( indexOfField[ 1 ] * 10 ) + indexOfField[ 2 ] ) ) / 100;

    sumOfIndexes = 2 + edge;

    for ( i = 1; i <= edge; i++ )
    {
        amountOfFields += i;
    }

    minLevel = amountOfFields - 2;

    printf( TEXT_FIELDS_AND_POSSIBLE_MOVES );
    pFirstMove = malloc( sizeof( possMove ) );
    pPossMove = pFirstMove;

    for ( i = 1; i <= edge; i++ )
    {
        for ( j = 1; j <= edge; j++ )
        {
            for ( k = 1; k <= edge; k++ )
            {
                if ( ( i + j + k ) == sumOfIndexes )
                {
                    counter++;
                    AllFields[i][j][k] = 1 ;
                    printf( "\n%2d. %d%d%d %d -> ", counter, i, j, k, AllFields[i][j][k] );
                    
                    if ( ( i + 1 ) <= edge && j >= 2 && i >= 2 && ( j + 1 ) <= edge )
                    {
                        counterA++;
                        pPossMove->move[3] = i;
                        pPossMove->move[4] = j;
                        pPossMove->move[5] = k;
                        pPossMove->move[0] = i - 1;
                        pPossMove->move[1] = j + 1;
                        pPossMove->move[2] = k;
                        pPossMove->move[6] = i + 1;
                        pPossMove->move[7] = j - 1;
                        pPossMove->move[8] = k;
                        printMove( pPossMove->move );
                        pPossMove->pNext = malloc( sizeof( possMove ) );
                        pPossMove = pPossMove->pNext;
                    }
                    if ( ( i + 1 ) <= edge && k >= 2 && i >= 2 && ( k + 1 ) <= edge )
                    {
                        counterA++;
                        pPossMove->move[3] = i;
                        pPossMove->move[4] = j;
                        pPossMove->move[5] = k;
                        pPossMove->move[0] = i - 1;
                        pPossMove->move[1] = j;
                        pPossMove->move[2] = k + 1;
                        pPossMove->move[6] = i + 1;
                        pPossMove->move[7] = j;
                        pPossMove->move[8] = k - 1;
                        printMove( pPossMove->move );
                        pPossMove->pNext = malloc( sizeof( possMove ) );
                        pPossMove = pPossMove->pNext;                        
                    }
                    if ( ( k + 1 ) <= edge && j >= 2 && k >= 2 && ( j + 1 ) <= edge )
                    {
                        counterA++;
                        pPossMove->move[3] = i;
                        pPossMove->move[4] = j;
                        pPossMove->move[5] = k;
                        pPossMove->move[0] = i;
                        pPossMove->move[1] = j + 1;
                        pPossMove->move[2] = k - 1;
                        pPossMove->move[6] = i;
                        pPossMove->move[7] = j - 1;
                        pPossMove->move[8] = k + 1;
                        printMove( pPossMove->move );
                        pPossMove->pNext = malloc( sizeof( possMove ) );
                        pPossMove = pPossMove->pNext;                        
                    }
                }
            }
        }
    }

    pPossMove->pNext = NULL;

    printf( TEXT_SUMMARY_OF_POSSIBLE_MOVES );
    printOfPossibleMoves( pFirstMove );

    amountOfMoves = counterA;
    printf( TEXT_AMOUNT_OF_POSSIBLE_MOVES, amountOfMoves );

    if ( AllFields[ indexOfField[ 0 ] ][ indexOfField[ 1 ] ][ indexOfField[ 2 ] ] == 1 )
    {
        AllFields[ indexOfField[ 0 ] ][ indexOfField[ 1 ] ][ indexOfField[ 2 ] ] = 0;
    }
    else
    {
        printf( ERROR_MSG_BAD_START_POSITION );
        return ERROR_NUM_BAD_START_POSITION;
    }

    counter = 0;
    pFirstLevel = malloc( sizeof( possMove ) );
    pRealMove = pFirstLevel;

    while ( end == 0 )
    {
        counter++;
        
        if ( ( counter % 100 ) == 0 )
        {
            printf( "." );
        }
        if ( ( counter % 1000 ) == 0 )
        {
            printf( "\n%d ", counter );
        }
        
        countOfMoves = 0;
        
        /* ---- Set beginning of all possible move.
         */
        pPossMove = pFirstMove;

        /* ---- Set beginning of new level.
         */
        assert( pRealMove != NULL );
        
        pFirstMoveInLevel = pRealMove;

        /* ---- Search for new possible moves in current state
         *      of game.
         */
        while ( pPossMove->pNext != NULL ) 
        {
            suma = 0;
            suma += AllFields[ pPossMove->move[0] ][ pPossMove->move[ 1 ] ][ pPossMove->move[ 2 ] ] * 1;
            suma += AllFields[ pPossMove->move[3] ][ pPossMove->move[ 4 ] ][ pPossMove->move[ 5 ] ] * 2;
            suma += AllFields[ pPossMove->move[6] ][ pPossMove->move[ 7 ] ][ pPossMove->move[ 8 ] ] * 4;
           
            /* ---- Test, if move is playable. For 3 or 6 is playable.
             */
            if ( suma == 3 || suma == 6 )
            {
                
                /* ---- Increase count of the realised moves.
                 */
                countOfMoves++;
                    
                /* ---- Assign playable move to game moves realisation list.
                 */
                assignMove( pPossMove->move, pRealMove->move );

                /* ---- Assign pointer to pPrevious record to current record.
                 */
                pRealMove->pPrev = pTempMove;
                    
                /* ---- Assign to pTempMove pointer to current record.
                 */
                pTempMove = pRealMove;
                    
                /* ---- Allocation of memory for new record.
                 */
                pRealMove->pNext = malloc( sizeof( realMove ) );
                //printf( "\n pRM-MAL:%x \n", pRealMove->pNext );
                    
                pRealMove->pBeginningOfLevel = NULL;
                    
                /* ---- Set pointer to newly created record.
                 */
                pRealMove = pRealMove->pNext;
                pRealMove->pNext = NULL; 
                    
                pRealMove->pPrev = pTempMove;
            }

            pPossMove = pPossMove->pNext;
        }

        /* ---- New moves were found in higher level
         */

        if ( countOfMoves > 0 )
        {
            /* ---- Set pointer of last move in level to first move in level.
             */
            pTempMove->pBeginningOfLevel = pFirstMoveInLevel;

            /* ---- Set pTempAMove to point on first move in level.
             */
            pTempAMove = pTempMove->pBeginningOfLevel;
            
            /* ---- Increase level.
             */
            level++;
            
            /* ---- adjust boundaries of inside window
             */
            setBoundaries( &minLevel, &lastMinLevel, level );
            //printf( "\n|(+)L%d---min(%d)-max(%d)---|\n", level, minLevel, maxLevel);

            boundaryCounter++;


            /* ---- Proceed pNext move ( first in newly found level )
             */
            doMove( AllFields, pTempAMove->move );
            //printStateOfGame( pFirstLevel );

            /* ---- This level contains last move, what means
             *      that we have the result.
             */
            if ( level == ( amountOfFields - 2 ) )
            {
                amountOfResults++;

                if ( amountOfResults > PRINT_RESULT_FOR_EACH ) 
                {
                    if ( ( amountOfResults % PRINT_RESULT_FOR_EACH2 ) == 0 ) 
                    {
                        printf( TEXT_RESULT_WAS_FOUND, amountOfResults );
                        printStateOfGame( pFirstLevel );
                    } else {
        	           printf( "R" );
                    }
                } 
                else 
                {
                    printf( TEXT_RESULT_WAS_FOUND, amountOfResults );
                    printStateOfGame( pFirstLevel );
                }

            }
            
        }
        else
        /* ---- New moves were not found, go to pPrevious state of 
         *      game.
         */
        {
            /* ---- Proceed step back
             */
            oldLevel = level;

            /* ---- Realisation of back step. 
             *          AllFields    - array of fields,
             *          pTempMove   - pointer to last record in level with
             *                    playbable move,
             *          &level - pointer to number of current level.
             */
            if ( level == 0 ) {

                printf( TEXT_REPORT_SOLUTION, amountOfResults );
                printf( TEXT_REPORT_OF_TOTAL_FORWARD_LEVELS, counter );
                return 0;
            }

            /* ---- move backwards
             *
             */
            backStep( AllFields, pTempMove, &level, counter );

    	    if ( level == 0 ) {

    	        printf( TEXT_REPORT_SOLUTION, amountOfResults );
    	        printf( TEXT_REPORT_OF_TOTAL_FORWARD_LEVELS, counter );
        		return 0;
    	    }

            /* ---- Set pointers, if level is less then before 
             *      back steps begin.
             */

            if ( ( oldLevel - level ) > 0 )
            {
                for ( i = 0; i < ( oldLevel - level ); i++ )
                {
                    pTempAMove = pRealMove;
                    pRealMove = pRealMove->pPrev;
                    pTempMove = pRealMove->pPrev;
                    pRealMove->pNext = NULL;
                    free( pTempAMove );
                }

                if ( level == 1 ) {
                    pFirstLevel = pTempMove->pBeginningOfLevel;
                    pFirstLevel->pPrev=NULL;
                }
            }
        }

        /* --- introduce inside boundaries heuristic function
         * 
         */
        setBoundaries( &minLevel, &lastMinLevel, level );

        //printf( "\n|(-)L%d---boundary---lmin(%d)---min(%d)----|\n", level, lastMinLevel, minLevel);

        if ( boundaryCounter > BOUNDARIES_RANGE ) {

            printf( "\n|--------------boundary found-----step(%d)----min(%d)---|\n", counter, minLevel);
            fflush(stdout);

            boundaryCounter = 0;
            //minLevel = amountOfFields - 2;
            lastMinLevel = 0;

            //printStateOfGame(pFirstLevel);

            while ( 1 == 1 ){

                oldLevel = level;
                //printf( "\n|---before-BS---L(%d)--minL(%d)\n", level, minLevel);
                //fflush(stdout);
                backStep( AllFields, pTempMove, &level, counter );
                //printf( "\n|---after--BS---L(%d)--minL(%d)\n", level, minLevel);
                //fflush(stdout);

                if ( ( oldLevel - level ) > 0 )
                {
                    for ( i = 0; i < ( oldLevel - level ); i++ )
                    {
                        pTempAMove = pRealMove;
                        pRealMove = pRealMove->pPrev;
                        pTempMove = pRealMove->pPrev;
                        pRealMove->pNext = NULL;
                        free( pTempAMove );
                    }

                    if ( level == 1 ) {
                        pFirstLevel = pTempMove->pBeginningOfLevel;
                        pFirstLevel->pPrev=NULL;
                    }
                }

                /* --- end zero level
                 */
                if ( level == 0 ) {

                    printf( TEXT_REPORT_SOLUTION, amountOfResults );
                    printf( TEXT_REPORT_OF_TOTAL_FORWARD_LEVELS, counter );
                    return 0;
                }                

                if ( level <= minLevel ){
                    //printf( "\n|---escape-BS-L(%d)\n", level);
                    //printStateOfGame(pFirstLevel);
                    //fflush(stdout);
                    minLevel = level;
                    break;
                }

                //printStateOfGame(pFirstLevel);
                //fflush(stdout);

            }
        }
    }
}
